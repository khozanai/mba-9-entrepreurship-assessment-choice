<?php

/**
 *
 * @package    local_mbaentrepreneurship
 * @subpackage mbaentrepreneurship
 * @copyright  2017 Raymond Mlambo, khozanai@gmail.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');
require_once 'lib.php';

$id = optional_param('id', 0, PARAM_INT);
$course = $DB->get_record('course', array('id' => $id));
$context = context_course::instance($course->id, MUST_EXIST);
require_login();
$PAGE->set_context($context);
$PAGE->set_url('/local/mbaentrepreneurship/data.php', array(
    'id' => $course->id
));
$PAGE->set_pagetype('course-view-' . $course->format);
$PAGE->set_title($course->shortname);
$PAGE->set_pagelayout('course');
$PAGE->navbar->add('Programme', new moodle_url('/local/programme', array(
    'id' => 3
)));
$PAGE->navbar->add('MBA', new moodle_url('/course/index.php', array(
    'categoryid' => 3
)));
$PAGE->navbar->add($course->fullname, new moodle_url('/course/view.php', array(
    'id' => 63
)));
$PAGE->requires->js('/local/mbaentrepreneurship/mbaentrepreneurship.js');
echo $OUTPUT->header();

if (enrolled_student($USER->id, $course->id)) {
    // THIS IS A STUDENT VIEW
    $data = $DB->get_record('local_mbaentrepreneurship', array('userid' => $USER->id));


    echo html_writer::start_tag('div', array('class' => 'container'));
    echo 'Hello ' . $USER->firstname;
    echo html_writer::empty_tag('br');
    echo html_writer::empty_tag('br');
    if (empty($data)) {
        echo 'You haven\'t made your selection yet.';
        echo html_writer::end_tag('div');
        echo $OUTPUT->footer();
        die();
    }
    echo 'This was the selection that you made for your ' . $course->fullname . ' assessment';

    $choice = '';
    if ($data->exam_equivalent == 1) {
        $choice = 'Exam equivalent';
    } else {
        $choice = 'Assignment';
    }
    echo html_writer::empty_tag('br');
    echo html_writer::empty_tag('br');
    $table = new html_table();
    $table->head = array('Name', 'Choice', 'Date');
    $row = new html_table_row(array(
        fullname($USER),
        $choice,
        date('d M Y H:i:s', $data->timecreated)
    ));
    $row->attributes['class'] = '';
    $table->data[] = $row;
    echo html_writer::table($table);

    echo html_writer::end_tag('div');
} else {
    // THIS VIEW IS FOR EVERYONE ELSE WHO ISN'T A STUDENT
    echo html_writer::start_tag('div', array('class' => 'container'));

    // download button 
    echo html_writer::tag('button', 'Download data to a spreadsheet', array('class' => 'button-download'));
    echo html_writer::empty_tag('br');
    echo html_writer::empty_tag('br');

    $data = $DB->get_records('local_mbaentrepreneurship');
    $table = new html_table();
    $table->head = array('Name', 'Choice', 'Date');
    foreach ($data as $datum) {
        $student = $DB->get_record('user', array('id' => $datum->userid));
        $choice = '';
        if ($datum->exam_equivalent == 1) {
            $choice = 'Exam equivalent';
        } else {
            $choice = 'Assignment';
        }
        $row = new html_table_row(array(
            fullname($student),
            $choice,
            date('d M Y H:i:s', $datum->timecreated)
        ));
        $row->attributes['class'] = '';
        $table->data[] = $row;
    }

    echo html_writer::table($table);
    echo html_writer::end_tag('div');
}

echo $OUTPUT->footer();
