<?php

/**
 * The purpose of this plugin is to enable students 
 * the ability to fill out a form with their exam venue preference and mode of exam
 *
 * @package    local
 * @subpackage examvenue
 * @copyright  2017 Raymond Mlambo, khozanai@gmail.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * 
 */
defined('MOODLE_INTERNAL') || die;

function local_mbaentrepreneurship_extend_settings_navigation($settingsnav, $context) {
    global $CFG, $PAGE;
    // Only add this settings item on non-site course pages.
    if (!$PAGE->course or $PAGE->course->id == 1) {
        return;
    }
    // Only let users with the appropriate capability see this settings item.
    if (!has_capability('moodle/backup:backupcourse', context_course::instance($PAGE->course->id))) {
        // return;
    }

    if ($settingnode = $settingsnav->find('courseadmin', navigation_node::TYPE_COURSE)) {
        $mbaentrepreneurshipsettings = get_string('mbaentrepreneurshipsettings', 'local_mbaentrepreneurship');
        $url = new moodle_url('/local/mbaentrepreneurship/data.php', array('id' => $PAGE->course->id));
        if ($PAGE->course->id == 63) { // this is the module that I'm targeting
            $mbaentrepreneurship = navigation_node::create(
                            $mbaentrepreneurshipsettings, $url, navigation_node::NODETYPE_LEAF, 'mbaentrepreneurship', 'mbaentrepreneurship', new pix_icon('t/addcontact', $mbaentrepreneurshipsettings)
            );
            if ($PAGE->url->compare($url, URL_MATCH_BASE)) {
                $mbaentrepreneurshipsettings->make_active();
            }
            $settingnode->add_node($mbaentrepreneurship);
        }
    }
}

/**
 * 
 * @global type $DB
 * @global type $PAGE
 * @param int $userid
 * @param int $courseid
 * @return bool true if the student is enrolled into module
 */
function enrolled_student($userid, $courseid) {
    global $DB, $PAGE;
    $enrolled = FALSE;
    if ($courseid == 63) {
        $sql = "SELECT ue.userid FROM {enrol} e INNER JOIN {user_enrolments} ue ON ue.enrolid = e.id WHERE e.courseid = ?"
                . " AND ue.userid = ?";
        $students = $DB->get_records_sql($sql, array($courseid, $userid));
        foreach ($students as $student) {
            if (!empty($student) && !has_capability('moodle/backup:backupcourse', context_course::instance($courseid))) {
                return $enrolled = TRUE;
                break;
            }
        }
    }
}

/**
 * 
 * @global type $DB
 * @global type $PAGE
 * @param type $userid
 * @return boolean whether the student has made a choice or not
 */
function has_made_a_choice($userid) {
    global $DB, $PAGE;
    $has_made_a_choice = FALSE;
    if (($DB->record_exists('local_mbaentrepreneurship', array(
                'userid' => $userid,
                'course' => $PAGE->course->id
            )))) {
        return $has_made_a_choice = TRUE;
    } else {
        return $has_made_a_choice;
    }
}

/**
 * 
 * @global type $DB
 * @param int $student The student's userid
 * @param int $choice The student's choice
 */
function add_to_group($student, $choice) {
    global $DB;
    $exam_groupid = 3068;
    $assignment_groupid = 3069;

    $record = new stdClass();
    $record->id = '';
    if ($choice == 1) {
        $record->groupid = $exam_groupid;
    } else {
        $record->groupid = $assignment_groupid;
    }
    $record->userid = $student;
    $record->timeadded = time();

    $DB->insert_record('groups_members', $record);
}

/**
 * 
 * @global type $CFG
 * @global type $USER
 * @global type $DB
 * @param type $userid
 * @param type $choice
 */
function send_notification($userid, $choice) {
    global $CFG, $USER, $DB;
    require_once("$CFG->libdir/moodlelib.php");
    $c = '';
    if ($choice == 1) {
        $c = 'Exam Equivalent';
    } else {
        $c = 'Assignment';
    }
    $student = $DB->get_record('user', array('id' => $userid));
    $subject = fullname($student) . ': MBA Entrepreneurship assessment choice';
    $message = 'Hello ' . fullname($student) . ', '
            . 'This is notify you of the choice that you\'ve just made for your MBA Entrepreneurship assessment.'
            . 'You selected: ' . $c . '. '
            . 'Warm Regards,'
            . 'Regenesys Business School';
    $support_user = $CFG->supportemail;
    $from_system = core_user::get_support_user();
    // email_to_user($student, $from_system, $subject, html_to_text($message));
    // e-mail the teacher a copy as well??!!
    $admin = $DB->get_record('user', array('id' => 49944));
    email_to_user($admin, $from_system, $subject, html_to_text($message));
}
