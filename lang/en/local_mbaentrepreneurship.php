<?php

// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * * The purpose of this plugin is to enable students 
 * the ability to fill out a form with their exam venue preference and mode of exam
 *
 * @package    local
 * @subpackage mbaentrepreneurship
 * @copyright  2017 Raymond Mlambo, khozanai@gmail.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['examvenue_choose'] = 'Choose your exam venue here';
$string['programme'] = 'MBA Programme';
$string['text'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
        . 'ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco '
        . 'laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit '
        . 'in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat'
        . ' cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
$string['mbaentrepreneurship'] = 'View MBA Entrepreneurship Data';
$string['mbaentrepreneurshipsettings'] = 'MBA9 Entrepreurship option';



