<?php

// This file is part of the Local examvenue plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_mbaentrepreneurship
 * @subpackage mbaentrepreneurship
 * @copyright  2017 Raymond Mlambo, khozanai@gmail.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');
require_once 'lib.php';


if (filter_input(INPUT_POST, 'submit')) {
    $choice = filter_input(INPUT_POST, 'choice');
    $courseid = filter_input(INPUT_POST, 'courseid');

    if (!empty($choice) && $courseid == 63) {

        $response = new stdClass();
        $response->id = '';
        $response->course = $courseid;
        $response->userid = $USER->id;
        if ($choice == 1) {
            $response->exam_equivalent = 1;
        } else {
            $response->assignment = 1;
        }
        $response->timecreated = time();
        $save = $DB->insert_record('local_mbaentrepreneurship', $response);
        if ($save) {
            // do something in lib.php, then redirect the student to the course page
            add_to_group($USER->id, $choice);
            // send_notification($USER->id, $choice);
            redirect(new moodle_url('/course/view.php', array('id' => $courseid)));
        }
    } else {
        // throw an exception here
        throw new RuntimeException('Invalid data has been passed to the page, please contact your admin');
    }
} else { // Nothing has been submitted yet from the form
    $courseid = optional_param('courseid', 0, PARAM_INT);
    $userid = optional_param('userid', 0, PARAM_INT);

    if ($courseid !== 63) {
        // throw an exception here
        throw new moodle_exception(' Invalid course has been passed to the page. Please make sure that you\'re on an MBA'
        . ' Entrepreneurship page.');
    }
    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
    $student = $DB->get_record('user', array('id' => $userid));

    $context = context_course::instance($course->id, MUST_EXIST);

    require_login($course);
    if (!is_siteadmin()) {
        // well, not gonna bother with this!
    }
    $PAGE->set_context($context);
    $PAGE->set_url('/local/mbaentrepreneurship', array(
        'courseid' => $course->id,
        'userid' => $student->id
    ));
    $PAGE->set_heading($course->fullname);
    $PAGE->set_pagelayout('course');
    $PAGE->set_title('MBA Entrepreneurship: Choice');
    $PAGE->navbar->add(fullname($student) . ': Make Your Assessment Choice', new moodle_url('/local/mbaentrepreneurship', array(
        'courseid' => $course->id,
        'userid' => $student->id
    )));
    $PAGE->requires->js('/local/mbaentrepreneurship/mbaentrepreneurship.js');

    /**
     * REDIRECT THE STUDENT OFF THE PAGE IF HE HAS ALREADY MADE A CHOICE
     */
    if (enrolled_student($USER->id, $course->id)) {
        if ($DB->record_exists('local_mbaentrepreneurship', array(
                    'userid' => $USER->id
                ))) {
            redirect(new moodle_url('/course/view.php', array('id' => 63)));
            die();
        }
    }

    echo $OUTPUT->header();

    echo html_writer::start_tag('div', array('class' => 'container'));
    echo $OUTPUT->heading($course->fullname);

    // echo html_writer::tag('div', get_string('text', 'local_mbaentrepreneurship'));

    echo '<div>'
    . ''
    . 'Please indicate, by the end of Wednesday (August 2) whether you will be doing the exam equivalent assessment
                for your MBA Entrepreneurship course, or the individual assignment with the exam. <br /><br /> 

Note that all students need to complete the digital assessments and the group assignment for this course.
'
    . '<br /><br />'
    . 'A reminder of what is required for each choice:'
    . '<br /><br />'
    . '<table style="width: 95%; height: auto; border: 0px;">'
    . '<tr style="border: 0px;">'
    . '<td style="font-weight: bold; width: 45%;">Individual assignment and exam</td>'
    . '<td style="font-weight: bold;">Exam equivalent assessment</td>'
    . '</tr>'
    . '<tr style="border: 0px;">'
    . '<td><ul><li>Submit your assignment on August 8 2017</li></ul></td>'
    . '<td><ul><li>Submit your exam equivalent assignment on December 31 2017</li></ul></td>'
    . '</tr>'
    . '<tr style="border: 0px;">'
    . '<td><ul><li>Write your exam on November 11 2017</li></ul></td>'
    . '<td></td>'
    . '</tr>'
    . '<tr style="border: 0px;">'
    . '<td></td>'
    . '<td style="font-weight: bold;">Note that:</td>'
    . '</tr>'
    . '<tr style="border: 0px;">'
    . '<td></td>'
    . '<td><ul><li>You must start creating your project report on Day 1.</li></ul></td>'
    . '</tr>'
    . '<tr style="border: 0px;">'
    . '<td></td>'
    . '<td><ul><li>If mentoring, you must provide at least one hour a month of mentoring to a new venture assigned by '
    . 'Shanduka OR another organisation of your choice and, as part of your submission, report back on the mentoring process.</li></ul></td>'
    . '</tr>'
    . '<tr style="border: 0px;">'
    . '<td></td>'
    . '<td><ul><li>If you wish to work with a Shanduka venture, Shanduka will train you on mentoring.</li></ul></td>'
    . '</tr>'
    . '<tr style="border: 0px;">'
    . '<td></td>'
    . '<td><ul><li>Your advice to the new venture or entrepreneur must be based on recognised management tools. </li></ul></td>'
    . '</tr>'
    . '<tr style="border: 0px;">'
    . '<td></td>'
    . '<td><ul><li>Your protégé will not be obliged to follow your advice.</li></ul> </td>'
    . '</tr>'
    . '<tr style="border: 0px;">'
    . '<td></td>'
    . '<td><ul><li>If you wish, you may continue to support Shanduka or the enterprise you have mentored beyond the assignment period. </li></ul> </td>'
    . '</tr>'
    . '</table>'
    . '<br /><br />'
    . 'Results for all students will be released after all assignments have been marked.'
    . '<br /><br />'
    . 'Indicate your choice here:'
    . '</div>';



    echo html_writer::empty_tag('br');
    echo '<form action="index.php" method="post">'
    . '<br />'
    . '<input type="radio" name="choice" class="choice1" id="choice1" value="1"> Exam equivalent'
    . '<br /><br />'
    . '<input type="radio" name="choice" class="choice2" id="choice2" value="2"> Individual assignment and exam'
    . '<input type="hidden" name="courseid" id="courseid" value="' . $course->id . '">'
    . '<br /><br /><br />'
    . '<input type="submit" name="submit" id="submit-button" value="Save choice">'
    . '</form>';
    echo '<button id="save-button">Save choice</button>';
    echo html_writer::end_tag('div');

    echo $OUTPUT->footer();
}
