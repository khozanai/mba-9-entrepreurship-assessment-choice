<?php

/**
 *
 * @package    local_mbaentrepreneurship
 * @subpackage mbaentrepreneurship
 * @copyright  2017 Raymond Mlambo, khozanai@gmail.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../config.php');

$filename = 'filename=mbaentrepreneurship-student-choices.xls';
header("Content-Type: application/xls");
header("Content-Type: application/vnd.ms-excel");
// header("Content-Type: application/octet-stream");
header("Content-disposition: attachment; " . basename($filename));

echo 'Firstname' . "\t" . 'Lastname' . "\t" . 'Choice' . "\t" . 'Time' . "\n";

$data = $DB->get_records('local_mbaentrepreneurship');
foreach ($data as $datum) {
    $student = $DB->get_record('user', array('id' => $datum->userid));
    $choice = '';
    if ($datum->exam_equivalent == 1) {
        $choice = 'Exam equivalent';
    } else {
        $choice = 'Assignment';
    }
    echo $student->firstname . "\t" . $student->lastname . "\t" . $choice . "\t" . date('Y-m-d H:i:s', $datum->timecreated) . "\n";
}
readfile($filename);
exit();

