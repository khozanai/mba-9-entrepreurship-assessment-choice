/* global FALSE */

require(['jquery'], function ($) {
    $(document).ready(function () {

        $('.choice1').click(function () {
            $('#save-button').hide();
            $('#submit-button').show();
        });
        $('.choice2').click(function () {
            $('#save-button').hide();
            $('#submit-button').show();
        });

        $('.button-download').click(function () {
            window.location = "download.php";
        });

    });
});
