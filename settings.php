<?php

// This file is part of the Local examvenue plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The purpose of this plugin is to enable students 
 * the ability to fill out a form with their exam venue preference and mode of exam
 *
 * @package    local
 * @subpackage mbaentrepreneurship
 * @copyright  2017 Raymond Mlambo, khozanai@gmail.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

//$previewnode = $PAGE->navigation->add(get_string('programme', 'local_mbaentrepreneurship'), new moodle_url('/local/programme/view.php', array(
//    'id' => 3
//)), navigation_node::TYPE_CONTAINER);
//$thingnode = $previewnode->add(get_string('mbaentrepreneurship', 'local_mbaentrepreneurship'), new moodle_url('/local/mbaentrepreneurship/'));
//$thingnode->make_active();

if (!$hassiteconfig) {

    $moderator = get_admin();
    $site = get_site();

    $settings = new admin_settingpage('local_mbaentrepreneurship', get_string('pluginname', 'local_mbaentrepreneurship'));
    $ADMIN->add('localplugins', $settings);


 //Create the new settings page
   //  - in a local plugin this is not defined as standard, so normal $settings->methods will throw an error as
 //    $settings will be NULL
    $settings = new admin_settingpage('local_mbaentrepreneurship', 'mbaentrepreneurship settings page');
//
//    // Create 
//    $ADMIN->add('localplugins', $settings);
//
    // Add a setting field to the settings for this page
    $settings->add(new admin_setting_configtext(
            // This is the reference you will use to your configuration
            'local_mbaentrepreneurship_apikey',
            // This is the friendly title for the config, which will be displayed
            'External API: some regenesys api key here',
            // This is helper text for this config field
            'This is the key used to access the External API',
            // This is the default value
            'No Key Defined',
            // This is the type of Parameter this config is
            PARAM_TEXT
    ));

    $availablefields = new moodle_url('/local/mbaentrepreneurship/index.php');
}

