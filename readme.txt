This local plugin is developed to handle students who are enrolled into the MBA9 Entrepreneurship module 
for 2017.

The local plugin handles:
1) setup of the DB tables
2) installation of the functionality onto the system

Do the following to get the whole thing working:
1) install the plugin
2) Edit the files in /course/view.php file to trigger this plugin
3) Create a group manually and call it "MBA Entrepreneurship exam equivalent" and another one called "Assignment"
 -- EXAM EQUIVALENT -- This is the group that will house all the students who have chosen the exam equivalent
 -- ASSIGNMENT--  This is the group that will house all the students who have chosen the assignment
-- NB: Take note of the group ids, as they're needed in the lib.php file.