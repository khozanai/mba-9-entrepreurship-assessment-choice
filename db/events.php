<?php

// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * * The purpose of this plugin is to enable students 
 * the ability to fill out a form with their exam venue preference and mode of exam
 *
 * @package    local
 * @subpackage mbaentrepreneurship
 * @copyright  2017 Raymond Mlambo, khozanai@gmail.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

//$observers = array(
//    array(
//        'eventname' => '\core\event\user_created',
//        'callback' => '\local_mbaentrepreneurship\observer::send_welcome',
//    ),
//);
